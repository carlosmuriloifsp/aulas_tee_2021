import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(dividendo, divisor): number | string{
    if (divisor == 0){
      return 'Não existe divisão por zero';
    }
    return dividendo / divisor;
  }

  multiplica(input1, input2): number | string{
    return input1 * input2;
  };

  somar(input1, input2): number | string{
    
    return parseFloat(input1) + parseFloat(input2);
  };

}
