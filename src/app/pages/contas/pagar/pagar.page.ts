import { ContasService } from '../service/contas-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  listaCompras;

  constructor(
    private conta: ContasService
  ) { }

  ngOnInit() {
    this.conta.lista('pagar').
    subscribe(x => this.listaCompras = x);
  }

  remove(conta) {
    this.conta.remove(conta);
  }

}
