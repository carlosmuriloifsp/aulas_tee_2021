import { DateHelper } from '../../../helpers/DateHelper';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ContasService {
  collection: AngularFirestoreCollection;
  
  constructor(
    private db: AngularFirestore
  ) { }

  registraConta(conta){
    conta.id = this.db.createId();
    this.collection = this.db.collection('conta');
    return this.collection.doc(conta.id).set(conta);
  }

  lista(tipo){
    this.collection = this.db.collection('conta', ref=> ref.where('tipo', '==', tipo));
    return this.collection.valueChanges();
  }

  remove(conta){
    this.collection = this.db.collection('conta');
    this.collection.doc(conta.id).delete()
  }

  edita(conta) {
    this.collection = this.db.collection('conta');
    this.collection.doc(conta.id).update(conta);
  }

  /**
   * Totaliza as contas de acordo com seu tipo
   * @param tipo: String - modalidade de contas
   */
  total(tipo, date) {
    const aux = DateHelper.breakDate(date); 
    const ano = aux.ano;
    const mes = aux.mes;
    
    this.collection = this.db.collection('conta', ref => 
    ref.where('tipo', '==', tipo).where('mes', '==', mes).where('ano', '==', ano));
    return this.collection.get().pipe(map(snap => {
      let cont = 0;
      let sum = 0;

      snap.docs.map(doc => {
        const conta = doc.data();
        const valor = parseFloat(conta.valor);
        sum += valor;
        cont++;
      });

      return { num: cont, valor: new Intl.NumberFormat('pt-BR').format(sum) }
    }));
  }
}
