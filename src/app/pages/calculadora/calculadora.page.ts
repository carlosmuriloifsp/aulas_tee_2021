import { CalculadoraService } from '../../services/calculadora.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  titulo = 'Calculadora';
  form: FormGroup;
  resultado: string | number;


  constructor(
    private builder: FormBuilder,
    private calculadora: CalculadoraService
  ) { }

  ngOnInit() {
    this.form = this.builder.group({
      input1: ['', [Validators.required]],
      input2: ['', [Validators.required]]
    });
  }

  dividir(){
    const data = this.form.value;
    const input1 = data.input1;
    const input2 = data.input2;
    this.resultado = this.calculadora.divide(input1, input2);
  }

  multiplicar(){
    const data = this.form.value;
    const input1 = data.input1;
    const input2 = data.input2;

    this.resultado = this.calculadora.multiplica(input1, input2);
  }
  
  somar(){
    const data = this.form.value;
    const input1 = data.input1;
    const input2 = data.input2;

    this.resultado = this.calculadora.somar(input1, input2);
  }


}
